import logo from './logo.svg';
import './App.css';
import React,{useState} from 'react'
import axios from 'axios';


function App() {

  const [question, setQuestion] = useState("");
  const [reponse, setReponse] = useState("");


  const changeQuestion = (e)  => {
    setQuestion(e.target.value);
  };

  const changeReponse = (e)  => {
    setReponse(e.target.value);
  };

  const addQR = (event)  => {

    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append("question",question);
    formData.append("reponse", reponse);

    console.log(question);
    console.log(reponse);
    


    // Request made to the backend api
    // Send formData object
    axios.get("http://localhost:3000/qr/add",{
      params:{
        question : question,
        reponse: reponse,
      }
    })
        .then(function (response) {
            console.log(response);
        }).catch(function (error) {
            console.log(error);
        });
  };
  

  return (
    <div className="App">
      <h3>Ajout d'une question/reponse</h3>
      <form>
        <div>
          <label>Question :</label>
          <textarea name="question" onChange={changeQuestion}></textarea>
        </div>
        <div>
          <label>Reponse :</label>
          <textarea name="reponse" onChange={changeReponse}></textarea>
        </div>
        <br/>
        <button type="button" onClick={addQR}>Ajouter</button>
      </form>
      
    </div>
  );
}

export default App;
